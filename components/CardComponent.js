import React from 'react';
import { Image, StyleSheet, Text, View } from 'react-native';

class CardComponent extends React.Component {
    constructor(props){
        super(props)
           
    }

    render(){
        const item = this.props.item
        return (
            <View style={styles.bord}>
                <Image style={styles.image} source={{uri : item.picture}}/>
                <View key={item.id}>
                    <Text style={styles.text} >Nom : {item.name}</Text>
                    <Text style={styles.text} >Email : {item.email}</Text>
                    <Text style={styles.text} >Age : {item.age}</Text>
                </View>
                
            </View>
        )
    }
}

const styles = StyleSheet.create({
    text: {
      fontSize: 10,
      height: 20,
      justifyContent: "center",
      flexDirection: "row",
      textAlign: "left",
      paddingLeft: 10
    },

    image: {
        width: "100%",
        height: "100%",
        flex: 0.5,
        resizeMode: 'center',
        borderRadius: 2
    },

    bord: {
        flexDirection: "row",
        padding: 10,
        height: 80,
        borderWidth: 2,
        borderRadius: 6,
        borderColor: "black",
        margin: 10
    }
});

export default CardComponent
