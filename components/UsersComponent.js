import React from 'react';
import { Alert, Button, FlatList, ScrollView, StyleSheet, Text, View } from 'react-native';
import UserService from '../services/UserService';
import CardComponent from './CardComponent';


class UsersComponent extends React.Component {
    constructor(props){
        super(props)
                    
        /* REMEMBER USE this.setState for update THIS  */
        this.state = {
            usersData: [],
        }

        this.displayRandomUserAtRegularInterval()
    }
    
    displayRandomUserAtRegularInterval(){
        let usersFetch = new UserService([])
        let usersDataCloned = JSON.parse(JSON.stringify(this.state.usersData))
        setInterval(()=>{
            usersFetch.getRandomUser().then(user => {
                this.state.usersData.push(user);
                this.setState({
                    usersData: this.state.usersData,
                });
            })
            
        },1000);
    };

    clearBtnHandler(){
        this.setState({
            usersData: []
        });
        Alert.alert("List Erased !")
    }

    render(){
        return (
            <View style={styles.text}>
                <FlatList data={this.state.usersData} renderItem={renderItem} keyExtractor={ (item, index) => index.toString() } />
                <Button title="ERASE" onPress={this.clearBtnHandler.bind(this)}/>
            </View>
        )
    }
}



const renderItem = (props)=>(
    <CardComponent item={props.item} />
);

const styles = StyleSheet.create({
    text: {
      width: "100%",
      flex: 1
    }
});

export default UsersComponent
